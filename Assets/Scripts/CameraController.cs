﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform taget;
	public float limit = 10.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (taget.position.y - this.transform.position.y > limit) {
			this.transform.position = new Vector3 (this.transform.position.x, taget.position.y - limit, this.transform.position.z);
		}
	}
}
