﻿using UnityEngine;
using System.Collections;

public class DefindColor  {

	public enum ColorType { Yellow,Violet, Red, Blue };
	public static Color YellowColor = new Color (255.0f/255.0f, 255.0f/255.0f, 0);
	public static Color VioletColor = new Color (110.0f/255.0f, 0, 220.0f/255f);
	public static Color RedColor = new Color (255.0f/255.0f,0 , 90.0f/255.0f);
	public static Color BlueColor = new Color(88.0f/255.0f,237.0f/255.0f,212.0f/255.0f);


	public static Color getColor ( ColorType colorType)
	{

		switch(colorType)
		{
		case ColorType.Yellow:
			return YellowColor;
		case ColorType.Red:
			return RedColor;
		case ColorType.Blue:
			return BlueColor;
		case ColorType.Violet:
			return VioletColor;
		}
		return Color.white;
	}

}
