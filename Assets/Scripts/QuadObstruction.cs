﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuadObstruction : MonoBehaviour {

	public List<Transform> lstTranform;
	public float distant = 0.5f;
	public float speed = 0.05f;
	public bool isTwoWays = false;
	public float widthScreen= 7.5f;
	public int tagetIndex = 0;
	public float tagetPosX;
	void Start () {
		LetStart ();
	}


	// Use this for initialization
	public void LetStart()
	{
		float startPos = distant * (1 - lstTranform.Count) * 0.5f; // 5 -> -2 distant, 4 -> -1.5 distant
		for (int i = 0; i < lstTranform.Count; i++) {
			if (lstTranform [i] != null)
				lstTranform [i].localPosition = new Vector2 (startPos + i*distant ,lstTranform[i].localPosition.y);
		}
		// create with Local position

		float height = (float)Screen.height;
		float width = (float)Screen.width;
		float ratio = height / width;
		widthScreen = 11.36f / ratio;
		speed = Mathf.Abs (speed);

		if(isTwoWays)
		{
			tagetIndex = 0;
			tagetPosX = -(4.262229f - distant * 0.5f); // static position with 4
		}
		else
		{
			tagetIndex = lstTranform.Count - 1;
			//tagetPosX = widthScreen / 2.0f + distant / 2.0f; //with ScreenSize

			tagetPosX = distant * (lstTranform.Count) * 0.5f;//static position without ScreenSize

		}
	}
	// Update is called once per frame
	void Update () {
		foreach (var tranform in lstTranform) {
			if (tranform != null) {
				tranform.localPosition = new Vector2 (tranform.localPosition.x + speed,tranform.localPosition.y);
			}

		}
		if(isTwoWays)
		{
			//if(Mathf.Abs(lstTranform[tagetIndex].position.x) <= Mathf.Abs(tagetPosX) && lstTranform[tagetIndex].position.x * speed > 0)
			if( (tagetPosX < 0.0f && lstTranform[tagetIndex].localPosition.x > tagetPosX)
				|| (tagetPosX > 0.0f && lstTranform[tagetIndex].localPosition.x < tagetPosX))
			{
				speed = speed * -1.0f;
				tagetPosX = tagetPosX * -1.0f;
				tagetIndex = Mathf.Abs (tagetIndex - (lstTranform.Count - 1));
			}
		}
		else
		{
			if(lstTranform[tagetIndex].localPosition.x >= tagetPosX)
			{
				lstTranform [tagetIndex].localPosition =
					new Vector2 (lstTranform [tagetIndex].localPosition.x - distant*(lstTranform.Count), 
									lstTranform[tagetIndex].localPosition.y);
				tagetIndex = (tagetIndex + lstTranform.Count - 1) % lstTranform.Count;

			}
		}
	}
}
