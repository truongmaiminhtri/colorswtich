﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainGameController : MonoBehaviour {
	int countStar = 0;
	public Text text;
	public List<GameObject> lstCurrent;
	// Use this for initialization

	void Start () {
		Global.gameController = this;
		text.text = "0";
		lstCurrent = new List<GameObject> ();
		cameracontroller = CameraTransform.GetComponent<CameraController> ();

	}
	public float nextPosition = 0.0f;
	public float finalPosition = 0.0f;
	public Transform CameraTransform;
	public CameraController cameracontroller = null;
	
	// Update is called once per frame
	void Update () {
		if(finalPosition != 0.0f)
		{
			if (cameracontroller == null)
				cameracontroller = CameraTransform.GetComponent<CameraController> ();
			else {
				if (CameraTransform.position.y + cameracontroller.limit >= finalPosition) {
					WinRound ();
				}
			}
			
		}
		if (nextPosition - CameraTransform.position.y <= 5.68f) {
			addPrefab ();
		}

		if(lstCurrent.Count >= 1){
			GameObject headList = lstCurrent [0];
			if (headList.transform.position.y + headList.GetComponent<PrefabInfo> ().height < CameraTransform.position.y - 5.68f) {
				Destroy (headList);
				lstCurrent.RemoveAt (0);
			}
		}

	}

	public void addStar()
	{
		countStar++;
		this.text.text = countStar.ToString ();
	}

	void OnDestroy()
	{
		Global.gameController = null;
	}

	private bool iswaitingAddPrefab = false;
	private void addPrefab()
	{
		if(iswaitingAddPrefab)
			return;
		iswaitingAddPrefab = true;
		if (Global.prefabManager != null) {
			
			var gameObject = Global.prefabManager.getPrefab (countStar);
			lstCurrent.Add (gameObject);
			gameObject.transform.position = new Vector3 (0, nextPosition, 0);
			var prefinfo = gameObject.GetComponent<PrefabInfo> ();
			if (prefinfo != null)
				nextPosition += prefinfo.height;
			else
				Debug.Log ("missing PrefabInfo Compoment on prefab");

		}

		iswaitingAddPrefab = false;
	}

	void WinRound()
	{
		Destroy (this);
		Debug.Log ("win round");
	}
}
