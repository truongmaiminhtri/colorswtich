﻿using UnityEngine;
using System.Collections;

public class AutoRotation : MonoBehaviour {

	public bool isClockwise = false;
	public bool isPausing = false;
	public float speed = 0.01f; 
	// Use this for initialization
	
	// Update is called once per frame
	void Update () {
		if (isPausing)
			return;
		if(!isClockwise)
			this.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z + speed);
		else
			this.transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z - speed);
		
	}
}
