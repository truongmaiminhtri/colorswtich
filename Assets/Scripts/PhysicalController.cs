﻿using UnityEngine;
using System.Collections;
using UniRx; 
using System;

public class PhysicalController : MonoBehaviour {
	public DefindColor.ColorType colorType;

	public GameObject particalSystem;
	private float camerasize = 5.68f;
	public Transform camera;
	public Transform deadLine;
	//public float height;
	//public float width;
	//private float ratio;
	private float distant;
	
	public  float g = -20f;
	public  float defaultForce = 6.32455f;
	private float v0 = 0.0f;

	private float t = 0.0f;
	private float s0;


	public float hMax = 1.0f;


	public Vector2 startPos = Vector2.one;
	public bool isPausing = false;// pause game
	public bool isStarted = false;//pass the tutorial
	public bool isFreezing = true;// freeze Position when in tutorial
	public float tutorialLimit = 2.0f;
	// Use this for initialization
	void Start () {

		particalSystem.SetActive (false);
		Global.ball = this;
		changeColor (DefindColor.ColorType.Red);
		distant = -(10.0f / 3.0f) * ((float)Screen.height/(float)Screen.width) ;
		v0 = defaultForce;
		s0 = startPos.y;
		t = 0.0f;
		startPos =  new Vector2 (0, -3.4f);
		transform.position = startPos;
		//height = (float)Screen.height;
		//width = (float)Screen.width;
		//ratio = height / width;

	}


	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space))
			AddForce ();
		if (isPausing)
			return;
		CalcuPosition (Time.deltaTime);
	}

	private void CalcuPosition(float detaT)
	{
		if (isFreezing)
			return;
			
		t += detaT;

		float s = s0 + v0 * t + (g * t * t) / 2.0f;
		SetDeadline ();
		this.transform.position = new Vector3 (transform.position.x, s, transform.position.z);

		if (this.transform.position.y <= startPos.y) {
			if(isStarted == false)
			{
				this.transform.position = new Vector3 (transform.position.x, startPos.y, transform.position.z);
				isFreezing = true;
				return;
			}
		}

		if (this.transform.position.y <= deadLine.position.y) {
			if (isStarted == false) {
				this.transform.position = new Vector3 (transform.position.x, deadLine.position.y+1, transform.position.z);
				isFreezing = true;
				return;
			} else {
				Dead ();
			}
		}
	}

	public void SetDeadline()
	{
		deadLine.position = new Vector3 (camera.position.x, camera.position.y - camerasize , this.transform.position.z);
	}
		

	public void Dead()
	{
		var color = this.GetComponent<SpriteRenderer> ().color;
		color = new Color (color.r, color.g, color.b, 0);
		this.GetComponent<SpriteRenderer> ().color = color;
		particalSystem.SetActive (true);
		var autoCompoment = this.GetComponent<AutoRotation> ();
		autoCompoment.isPausing = true;
	
		isPausing = true;
		Observable.Timer (TimeSpan.FromSeconds (2.0f)).Subscribe (l => {
			isPausing = false;
		});
	}
	public void AddForce()
	{
		v0 = defaultForce;
		t = 0.0f;
		s0 = this.transform.position.y;
		isFreezing = false;
		if (isStarted == false && this.transform.position.y >= startPos.y + tutorialLimit) {
			isStarted = true;
		}
	}
	public void Reset()
	{
		v0 = defaultForce;
		t = 0.0f;
		transform.position = startPos;
		isPausing = false;// pause game
		isStarted = false;//pass the tutorial
		isFreezing = true;// freeze Position when in tutorial
		particalSystem.SetActive(false);
		var color = this.GetComponent<SpriteRenderer> ().color;
		color = new Color (color.r, color.g, color.b, 1.0f);
		this.GetComponent<SpriteRenderer> ().color = color;
	}
	public void ChangeVfromG()
	{
		defaultForce = Mathf.Sqrt (-2 * hMax * g);
	}
	public void ChangeGfromV()
	{
		g = (-1.0f * (defaultForce * defaultForce)) / (2 * hMax);
	}

	public void changeColor(DefindColor.ColorType colorType)
	{
		this.colorType = colorType;
		if (GetComponent<SpriteRenderer> () != null)
			GetComponent<SpriteRenderer> ().color = DefindColor.getColor (colorType);
	}

	void OnDestroy()
	{
		Global.ball = null;
	}
}