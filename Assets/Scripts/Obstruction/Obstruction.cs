﻿using UnityEngine;
using System.Collections;

public class Obstruction : MonoBehaviour {
	
	public DefindColor.ColorType colorType;

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (Global.ball == null)
			return;
		if (Global.ball.colorType != this.colorType) {
			Global.ball.Dead ();
		}
	}

	void Start () {
		var color = DefindColor.getColor (this.colorType);
		if (GetComponent<SpriteRenderer> () != null)
			GetComponent<SpriteRenderer> ().color = color;
		foreach (var sprt in GetComponentsInChildren<SpriteRenderer> (true)) {
			sprt.color = color;
		}
	}
}
