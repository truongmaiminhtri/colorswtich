﻿using UnityEngine;
using System.Collections;

public class Star : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) 
	{

		if (Global.gameController != null) {
			Global.gameController.addStar ();
		}		
		Destroy (this.gameObject);
	
	}
}
