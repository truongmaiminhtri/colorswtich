﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ChangeColorItem : MonoBehaviour {
	public DefindColor.ColorType colorDefault = DefindColor.ColorType.Yellow;
	public bool isRandom;

	public List<DefindColor.ColorType> lstColor;

	void OnTriggerEnter2D(Collider2D other) 
	{

		DefindColor.ColorType color = lstColor[Random.Range (0, lstColor.Count)];

		/*if (isRandom)
			color = (DefindColor.ColorType)Random.Range (0, 3);
		else
			color = colorDefault;*/
		other.gameObject.GetComponent<PhysicalController> ().changeColor(color);

		Destroy (this.gameObject);
	}
}
