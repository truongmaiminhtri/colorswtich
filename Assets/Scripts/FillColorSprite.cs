﻿using UnityEngine;
using System.Collections;


public class FillColorSprite : MonoBehaviour {

	public DefindColor.ColorType colorType;

	// Use this for initializationk
	void Start () {
		var color = DefindColor.getColor (this.colorType);
		if (GetComponent<SpriteRenderer> () != null)
			GetComponent<SpriteRenderer> ().color = color;
		foreach (var sprt in GetComponentsInChildren<SpriteRenderer> (true)) {
			sprt.color = color;
		}
	}

}
