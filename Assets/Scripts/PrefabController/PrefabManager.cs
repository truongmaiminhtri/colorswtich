﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrefabManager : MonoBehaviour {
	public GameObject defaultPrefab;
	public List<GameObject> lstPrefab;

	[SerializeField]
	public List<BunchOfPrefab> lstBunchPrefab;
	private int startIndex = 0;
	// Use this for initialization
	void Start()
	{
		Global.prefabManager = this;
		Debug.Log ("lstBunchPrefab: "+ lstBunchPrefab.Count);
		foreach (var i in lstBunchPrefab)
			i.Reset ();
	}

	public GameObject getPrefab(int CountStar)
	{
		for (int i = startIndex; i < lstBunchPrefab.Count; i++) {
			if (CountStar >= lstBunchPrefab [i].minstar && CountStar <= lstBunchPrefab [i].maxstar) {
				startIndex = i;
				return Instantiate( lstBunchPrefab [i].RandomPrefab() as GameObject);
			}
		}
		return Instantiate (defaultPrefab as GameObject);
	}

	public void Reset()
	{
		startIndex = 0;
		foreach (var i in lstBunchPrefab)
			i.Reset ();
	}
}
