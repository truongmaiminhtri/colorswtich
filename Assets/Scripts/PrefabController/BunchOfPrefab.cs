﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BunchOfPrefab {
	[SerializeField]
	public int minstar;
	[SerializeField]
	public int maxstar;
	[SerializeField]
	public List<GameObject> lstPrefab;
	[SerializeField]
	public List<GameObject> isLoaded;

	public void Reset()
	{
		if (isLoaded == null) {
			isLoaded = new List<GameObject> ();
			return;
		}
		foreach (var i in isLoaded) {
			lstPrefab.Add (i);
			isLoaded.Remove (i);
		}
	}
	public GameObject RandomPrefab()
	{
		if (lstPrefab.Count == 0) {
			Reset ();
		}
		int index = Random.Range (0, lstPrefab.Count);

		GameObject result = lstPrefab [index];
		lstPrefab.RemoveAt (index);
		isLoaded.Add (result);
		return lstPrefab [index];
	}
}
